LASSIM Toolbox
==============

About LASSIM and the LASSIM toolbox
-----------------------------------

Recent and ongoing improvements in measurements technologies have given the possibility 
to obtain systems wide omics data of several biological processes. However, the analysis of 
those data has to date been restricted to crude, statistical tools with important biological 
mechanisms, e.g. feedback loops, being overlooked. The omitting of such high influence details 
in a large scale network remains a major problem in today’s omics based environment and is a 
key aspect of truly understanding any complex disease. Therefore, we herein present the 
**LASSIM** (**LA**rge **S**cale **SI**mulation **M**odeling) toolbox for GRNs Inference, which revolves around the 
expansion of a well determined mechanistic ODE-model into the entire system.

With this toolbox, it is possible to run a default implementation of `lassim`, but also to
extend and improve its behaviour by creating new optimization algorithms, using a different 
system of ODEs, different types of integrators, and much more.

All the optimization algorithms currently available are implemented by [PyGMO](http://esa.github.io/pygmo/index.html) but there 
are no limitations on how the algorithms should be implemented, what it is important is to respect
the signatures of the classes that are part of the module `source/core`

**[Important]**
The project is in the beta release stage. It is working and currently under use inside our team but multiple tests are
still missing and the documentation is far from completed. Moreover, the general structure is still subject to 
important changes with a continuous refactoring of the entire codebase. If you have any feedback or you want to contribute
to the project, don't esitate to contact the current developers of it.

Current Members in the Project
------------------------------
- @gmariotti

How to install the toolbox
----------------------------------
Before using the the toolbox, be sure to satisfy all the requirements in the [Development environment and requirements](#development-environment-and-requirements). After you have done that, run the following command from a terminal:
```
git clone https://gitlab.com/Gustafsson-lab/lassim.git
cd lassim
./scripts/install.sh
```

How to use the toolbox
----------------------
For the core system optimization, the command is:
```
python lassim_core.py <configuration-file>
```
While for the peripheral genes optimization, the command is:
```
python lassim_peripherals.py <configuration-file>
```
or for running the peripheral genes optimization using MPI:
```
mpirun -n <n> python lassim_peripherals_mpi.py <configuration-file>
```

while for the list of terminal options availables use the command:
- `python lassim_core.py -h`
- `python lassim_peripherals.py -h`

Development environment and requirements
----------------------------------------
The current toolbox environment for its development and testing is:

- [Fedora 25 Workstation](https://getfedora.org/)
- [PyCharm 2016.x](https://www.jetbrains.com/pycharm/)
- [Anaconda 4.1.1](https://anaconda.org/) with Python 3.5.2
- [Boost 1.61.0](http://www.boost.org)
- [GSL 2.2.1](http://ftp.acc.umu.se/mirror/gnu.org/gnu/gsl/)
- [clang 3.8](http://clang.llvm.org/)

but is going to be tested and used mainly on the [NSC](https://www.nsc.liu.se) system available at the [Linköping University](http://liu.se/?l=en).

Instead, the list of **mandatory** dependencies is:

- [PyGMO 1.1.7](http://esa.github.io/pygmo/index.html)
- [NumPy 1.11.1/SciPy 0.17.1](http://www.scipy.org/)
- [pandas 0.18.1](http://pandas.pydata.org/)
- [Cython >= 0.24.0](http://cython.org/)
- [sortedcontainers >= 1.5.3](http://www.grantjenks.com/docs/sortedcontainers/)

Except for [sortedcontainers](http://www.grantjenks.com/docs/sortedcontainers/), all of them are already present in [Anaconda 4.1.1](https://anaconda.org/).

For tips on how to install [PyGMO](http://esa.github.io/pygmo/index.html), look at [INSTALL](INSTALL.md) file.

**[!]** clang compiler seems to be the one that gives less problems during the compilation process, 
but, even if not tested, there shouldn't be any issue with the gcc compiler too.

**[!]** Windows OS is not supported.

Python Version Supported/Tested
-------------------------------
- Python 3.5

What's next?
------------

- Making `source/core` a Python module installable from **PyPI**.
- New kind of base implementation for the optimization process, in order to use different algorithm 
in different ways.
- New formats for input data.
- Improvements on tests, documentation and code quality.
- A Gitter channel.

[Here](https://python3statement.github.io/) you can find one of the reasons why the support for Python 2.7 is highly improbable.

Current Branches
----------------

The `master` branch, usually, contains working code, tested on different environments. Check `releases` to see the 
latest stable version of the toolbox.

The `development` branch, instead, contains unstable, untested code, with future features and bug fixes, should not 
be used unless you want to help with the development. 

How to contribute
-----------------
In order to contribute to the project, it is necessary to first `fork` it. Then, it is important to decide on which branch 
to work on:
- `master` branch accepts only bug fixes and small corrections to the available code.
- `development` branch accepts bug fixes, corrections and the development of new features that can become part of
the `master` branch.

In all cases, for each pull request it would be nice to have some tests related to the code submitted and an explanation
of what it is submitted, for making the job easier for the repository maintainers.

References
----------
> **The generalized island model**, Izzo Dario and Ruci&#324;ski Marek and Biscani Francesco, Parallel Architectures and Bioinspired Algorithms, 151--169, 2012, Springer 
