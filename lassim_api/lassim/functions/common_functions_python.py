import numpy as np
# from scikits.odes import ode
from scipy import integrate

from ..type_aliases import Vector

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.3.0"


def lassim_function(y: Vector, t: float, lambdas: Vector, vmax: Vector,
                    k_map: Vector, ones: Vector, result: Vector):
    """
    Generates the list of functions to be "integrated"
    The form of the function is of the type
    dx/dt = -lambda * x + vmax / (1 + e-(SUMj kj * xj))

    :param y: value of each function during integration.
    :param t: time value at this point in the integration. Not used.
    :param lambdas: Vector of lambdas in form [lambda_1, lambda_2,.., lambda_n]
    :param vmax: Vector of vmax in form [vmax_1, vmax_2,.., vmax_n]
    :param k_map: a matrix representing all the reactions between all the
    transcription factors. Must be a (y.size, y.size) matrix.
    :param ones: Vector of ones, same size of lambdas and vmax, for performance
    purposes.
    :param result: Vector containing the result of this call. It must be of the
    same size of y0. Needed for performance purposes.
    :return: The value of the ode system at time t.
    """

    # don't worry about RuntimeWarning for np.exp overflow. Even if a value
    # become inf, because is at the denominator it will make the result equal
    # to 0
    np.add(
        np.multiply(np.negative(lambdas), y, out=result),
        np.divide(vmax, np.add(ones, np.exp(np.negative(np.dot(k_map, y))))),
        out=result
    )
    return result


def odeint1e8_lassim(y0: Vector, t: Vector, sol_vector: Vector, k_map: Vector,
                     k_map_mask: Vector, size: int, result: Vector):
    """
    ODE function wrapping a call to scipy.integrate.odeint with mxstep=1e8.

    :param y0: Values of y(0).
    :param t: Time sequence to evaluate.
    :param sol_vector: Solution vector for this system simulation.
    :param k_map: Map of the reactions as a vector. Must have size*size elements
    :param k_map_mask: Mask of the map of the reactions for map reshaping. Must
    have the same number of elements of k_map.
    :param size: Number of elements to consider.
    :param result: Vector containing the result of each integration call.
    Needed for performance purposes, it must of the same size of y0.
    :return: The values after integration of this system. The return values are
    the same of scipy.integrate.odeint
    """

    ones = np.ones(size)

    lambdas = sol_vector[:size]
    vmax = sol_vector[size: 2 * size]
    k_values = sol_vector[2 * size:]

    # map is a vector, but will be reshaped as a matrix size x size
    k_map[k_map_mask] = k_values
    return integrate.odeint(
        lassim_function, y0, t,
        args=(lambdas, vmax, np.reshape(k_map, (size, size)), ones, result),
        mxstep=int(1e8)
    )
