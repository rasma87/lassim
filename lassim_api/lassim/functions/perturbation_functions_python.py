import numpy as np
from numpy import linalg

from ..type_aliases import Vector

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.4.0"


def perturbation_func_sequential(pert_data: Vector, size: int, y0: Vector,
                                 sol_vector: Vector, vector_map: Vector,
                                 vector_map_mask: Vector, ode):
    """
    Sequential function for evaluation of the impact of the perturbations over
    the "normal" behaviour of the current system.

    :param pert_data: A size * size matrix representing the perturbations
        data.
    :param size: Number of elements to consider.
    :param y0: Starting values for integration.
    :param sol_vector: Current solution vector. Is responsibility of the ode
        to interpret it. The only supposition is that the perturbations diagonal
        influences the first num_tf values in the array.
    :param vector_map: Map for the ode.
    :param vector_map_mask: Mask of the map for the ode.
    :param ode: Function for performing the ode
    :return: Value of the impact of the perturbations over the "normal" system
    """

    sim_size = size * size
    control_dict = {}
    # first size elements of each row is the value of the perturbations,
    # while the remaining elements of each row are start time and end time
    perturbations = pert_data[:, :size]
    times = pert_data[:, size:]
    simulation = np.empty((sim_size, 1))
    ones = np.ones((sim_size, 1))
    pert_diag = np.diagonal(perturbations)
    res_container = np.empty(size)

    # number of columns of times
    t_size = times.shape[1]

    # at each iteration, the value i in the variations vector is modified
    # accordingly to the value in the diagonal.
    # example:
    # 0 -> [v1 1 ... 1]
    # 1 -> [v1 v2 ... 1]
    # ....
    # n-1 -> [v1 v2 v3 ... vn-1]
    for i in range(size):
        time_i = times[i]
        # FIXME - find a better way
        time_str = str(time_i)

        # the control_dict takes trace of control calculations already evaluated
        # for that time sequence. It should not impact performance considering
        # the fact that dictionaries are really fast in python
        if time_str not in control_dict:
            control_dict[time_str] = ode(
                y0, time_i, sol_vector, vector_map, vector_map_mask, size,
                res_container
            )
        sol_vector[i] = pert_diag[i] * sol_vector[i]
        temp_simul = ode(
            y0, time_i, sol_vector, vector_map, vector_map_mask, size,
            res_container
        )
        simulation[i * size: (i + 1) * size] = np.reshape(
            np.divide(
                temp_simul[t_size - 1],
                control_dict[time_str][t_size - 1]
            ), (size, 1)
        )

    np.subtract(simulation, ones, simulation)
    simulation[simulation > 2] = 2

    return linalg.lstsq(simulation, perturbations.flatten())[1][0]


def perturbation_peripheral(pert_data: Vector, size: int, y0: Vector,
                            sol_vector: Vector, pert_core: Vector,
                            vector_map: Vector, vector_map_mask: Vector,
                            ode):
    # the sim_size is equal to the number of data in pert_data, so equal to
    # the number of transcription factors
    sim_size = pert_data.shape[0]
    control_dict = {}

    perturbations = pert_core[:, :sim_size]
    times = pert_core[:, sim_size:]
    simulation = np.empty((sim_size, 1))
    ones = np.ones((sim_size, 1))
    pert_diag = np.diagonal(perturbations)
    res_container = np.empty(size)

    # number of columns of times
    t_size = times.shape[1]
    # at each iteration, the value i in the variations vector is modified
    # accordingly to the value in the diagonal.
    # example:
    # 0 -> [v1 1 ... 1]
    # 1 -> [v1 v2 ... 1]
    # ....
    # n-1 -> [v1 v2 v3 ... vn-1]
    for i in range(sim_size):
        time_i = times[i]
        # FIXME - find a better way
        time_str = str(time_i)

        # the control_dict takes trace of control calculations already evaluated
        # for that time sequence. It should not impact performance considering
        # the fact that dictionaries are really fast in python
        if time_str not in control_dict:
            control_dict[time_str] = ode(
                y0, time_i, sol_vector, vector_map, vector_map_mask, size,
                res_container
            )
        sol_vector[i] = np.multiply(sol_vector[i], pert_diag[i])
        temp_simul = ode(
            y0, time_i, sol_vector, vector_map, vector_map_mask, size,
            res_container
        )

        # from each simulation the only value to save is the one related to the
        # peripheral, not the ones related to the
        simulation[i] = np.divide(
            temp_simul[t_size - 1][sim_size],
            control_dict[time_str][t_size - 1][sim_size]
        )

    np.subtract(simulation, ones, simulation)
    simulation[simulation > 2] = 2
    residuals = linalg.lstsq(simulation, pert_data)[1]
    if residuals.size == 0:
        residuals = 0
    else:
        residuals = residuals[0]
    return residuals
