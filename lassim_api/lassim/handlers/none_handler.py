from sortedcontainers import SortedList

from ..solutions_handler import SolutionsHandler

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.1.0"


class NoneSolutionsHandler(SolutionsHandler):
    def handle_solutions(self, solutions: SortedList):
        pass
