import logging
import os
from concurrent.futures import ProcessPoolExecutor
from typing import List, Tuple, Callable, Iterable

from lassim.base_solution import BaseSolution
from lassim.solutions_handler import SolutionsHandler
from lassim.type_aliases import Tuple4V, Vector
from sortedcontainers import SortedList

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.4.0"


def plotting_function(data: Vector, data_time: Vector,
                      results: Vector, res_time: Vector,
                      x_label: str, y_label: str, figure_name: str):
    try:
        # to do in order to avoid the use of an X-server. For more info look at
        # http://stackoverflow.com/questions/4706451/how-to-save-a-figure-remotely-with-pylab/4706614#4706614
        import matplotlib
        matplotlib.use("Agg")

        import matplotlib.pyplot as plt
        plt.figure()
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(data_time, data, linestyle="None", marker="o", label="data")
        plt.plot(res_time, results, label="results")
        plt.legend()
        plt.savefig(figure_name)
        plt.close()
    except Exception:
        logging.getLogger(__name__).exception("Matplotlib exception")


def default_plotname_creator(directory: str, names: List[str],
                             solution: BaseSolution) -> Iterable[str]:
    pid = os.getpid()
    num_variables = solution.number_of_variables
    output_dir = "{}/{}_pid{}".format(directory, num_variables, pid)
    i = 0
    # avoid folders with same name
    while os.path.isdir(output_dir):
        output_dir += "_{}".format(i)
        i += 1
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    for name in names:
        yield "{}/{}_{}.png".format(output_dir, name, pid)


class PlotBestSolutionsHandler(SolutionsHandler):
    def __init__(self, directory: str, fig_names: List[str],
                 axis: List[Tuple[str, str]],
                 gres: Callable[[BaseSolution], Iterable[Tuple4V]],
                 name_creator: Callable[[str, List[str], BaseSolution],
                                        Iterable[str]]
                 = default_plotname_creator):
        if len(axis) != len(fig_names):
            raise ValueError("List of axis must have same size of names")
        if not os.path.isdir(directory):
            os.makedirs(directory)
            logging.getLogger(__name__).info(
                "Created directory {} for plots".format(directory)
            )

        self._directory = directory
        self._names = fig_names
        self._axis = axis
        self._gres = gres
        self._name_creator = name_creator

    def handle_solutions(self, solutions: SortedList, directory: str = None):
        outdir = self._directory
        if directory is not None and not os.path.isdir(directory):
            os.makedirs(directory)
            outdir = directory

        best_solution = solutions[0]
        generator = self._name_creator(outdir, self._names, best_solution)
        figures_names = [name for name in generator]

        # runs it on a different process in order to avoid problem if plotting
        # is not done on main thread
        with ProcessPoolExecutor() as pool:
            idx = 0
            futures = []
            for data, data_time, results, res_time in self._gres(best_solution):
                futures.append(pool.submit(
                    plotting_function, data, data_time, results, res_time,
                    self._axis[idx][0], self._axis[idx][1], figures_names[idx])
                )
                # out of bound check, avoids exception
                idx += 1
                if idx >= len(self._axis):
                    break
            # wait maximum 10 seconds for completion off each future otherwise
            # a TimeoutError is raised
            for future in futures:
                future.result(timeout=100)
