import numpy
from Cython.Build import cythonize
from Cython.Distutils import build_ext
from setuptools import setup, find_packages, Extension

__author__ = 'Guido Pio Mariotti'
__copyright__ = 'Copyright (C) 2016 Guido Pio Mariotti'
__license__ = 'GNU General Public License v3.0'
__version__ = '0.4.0'

compiler = {
    'build_ext': build_ext,
    'language_level': 3
}

extensions = [
    Extension('lassim.functions.perturbation_functions',
              ['lassim/functions/perturbation_functions.pyx'],
              include_dirs=[numpy.get_include()]),
    Extension('lassim.functions.common_functions',
              ['lassim/functions/common_functions.pyx'],
              include_dirs=[numpy.get_include()]),
]

with open("README.md") as readme:
    long_description = readme.read()

setup(
    name='lassim',
    version=__version__,
    url='https://gitlab.com/Gustafsson-lab/lassim',
    license=__license__,
    author=__author__,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Cython',
        'Programming Language :: Python :: 3.5'
    ],
    description='LASSIM API for GRN Inference',
    long_description=long_description,
    keywords='lassim grn modeling bioinformatics',
    packages=find_packages(exclude=["tests", "tests.*"]),
    cmdclass=compiler,
    ext_modules=cythonize(extensions),
    # PyGMO is a missing requirement because it cannot be found on pypi
    install_requires=[
        'Cython>=0.24.0',
        'matplotlib>=1.5.1',
        'numpy>=1.11.1',
        'pandas>=0.18.1',
        'scipy>=0.17.1',
        'sortedcontainers>=1.5.3'
    ]
)
