import os
from unittest import TestCase

from nose.tools import assert_equal, assert_raises

from customs.module_loader import ModuleLoader

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "1.0.0"

one_function = """
def funct(x):
    return "function executed with input {}".format(x)
"""

two_functions = """
def funct(x):
    return "function executed with input {} and output {}".format(x, _hidden(x))
    
def _hidden(x):
    return x * x
"""

single_class = """
class TestClass:
    def __init__(self, x):
        self.x = x
        
    def to_string(self):
        return "TestClass(x={})".format(self.x)
"""


class TestModuleImporter(TestCase):
    def setUp(self):
        self.test_file = "__test__file__.py"
        self.message = "Expected\n{}\nbut actual\n{}"

    def tearDown(self):
        if os.path.isfile(self.test_file):
            os.remove(self.test_file)

    def test_SingleFunctionImportedWithPath(self):
        with open(self.test_file, "w") as python_file:
            python_file.write(one_function)

        path = os.path.dirname(os.path.abspath(self.test_file))
        module_name = self.test_file.replace(".py", "")

        funct = ModuleLoader.load_reference(
            module_name, "funct", path, reload=True
        )
        expected = "function executed with input 10"
        actual = funct(10)
        assert_equal(expected, actual, self.message.format(expected, actual))

    def test_SingleFunctionImportedThatCallsAnother(self):
        with open(self.test_file, "w") as python_file:
            python_file.write(two_functions)

        path = os.path.dirname(os.path.abspath(self.test_file))
        module_name = self.test_file.replace(".py", "")

        funct = ModuleLoader.load_reference(
            module_name, "funct", path, reload=True
        )
        expected = "function executed with input 10 and output 100"
        actual = funct(10)
        assert_equal(expected, actual, self.message.format(expected, actual))

    def test_ExistingFunctionImportedFromModule(self):
        module_name = "os"
        funct = ModuleLoader.load_reference(module_name, "getcwd")
        expected = os.getcwd

        assert_equal(expected, funct, self.message.format(expected, funct))

    def test_NonExistingModule(self):
        module_name = "not_existing"
        assert_raises(
            ImportError, ModuleLoader.load_reference, module_name, "not_exist_f"
        )

    def test_ImportClassFromModule(self):
        with open(self.test_file, "w") as python_file:
            python_file.write(single_class)

        path = os.path.dirname(os.path.abspath(self.test_file))
        module_name = self.test_file.replace(".py", "")

        class_ref = ModuleLoader.load_reference(
            module_name, "TestClass", path, reload=True
        )
        expected = "TestClass(x=10)"
        class_instance = class_ref(10)
        actual = class_instance.to_string()
        assert_equal(expected, actual, self.message.format(expected, actual))
