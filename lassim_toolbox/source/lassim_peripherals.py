import logging
import os
import subprocess
import sys

import numpy as np
import pandas as pd
from typing import Tuple, List, Callable, Dict

try:
    from PyGMO import topology
except ImportError:
    logging.getLogger(__name__).exception("Error importing PyGMO")
    exit(1)

try:
    from lassim.factories import OptimizationFactory
    from lassim.handlers.csv_handlers import DirectoryCSVSolutionsHandler
    from lassim.lassim_context import LassimContext, OptimizationArgs, \
        ReferencesModule
    from lassim.solutions.peripheral_solution import PeripheralSolution
except ImportError:
    logging.getLogger(__name__).exception("Error importing lassim")
    exit(1)

from sortedcontainers import SortedDict, SortedList

from customs.configuration_custom import parse_peripherals_config, \
    default_terminal, peripheral_configuration_example, sections, \
    parse_modules_config
from customs.peripherals_creation import create_network, problem_setup, \
    parse_peripherals_data
from data_management.csv_format import parse_core_data
from utilities.configuration import ConfigurationParser, from_parser_to_builder
from utilities.data_classes import OutputFiles, InputFiles, CoreFiles, \
    ReferenceInfo

"""
Main script for handling the peripherals problem in the Lassim Toolbox.
Can also be used as an example on how the toolbox works and how the lassim_api
module can be integrated in an existing pipeline.
"""

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "1.0.0"


def load_modules(modules: List[ReferenceInfo]) -> ReferencesModule:
    # for now the module loaded are similar to the one in the lassim core
    # but it can change in the future
    from lassim_core import load_modules as load_modules_c
    return load_modules_c(modules)


def peripherals_job(files: InputFiles, core_files: CoreFiles,
                    output: OutputFiles, main_args: List[OptimizationArgs],
                    sec_args: List[OptimizationArgs],
                    modules: List[ReferenceInfo]
                    ) -> Tuple[List[PeripheralSolution], List[str]]:
    logger = logging.getLogger(__name__)

    # from the file corresponding to the lassim_api system, create the
    # CoreSystem and its values, then build the corresponding NetworkSystem
    network = create_network(files.network, core_files.core_system)
    core = network.core
    core_data = parse_core_data(core_files.core_system)

    references = load_modules(modules)
    context = LassimContext(
        network, references, main_args, sec_args
    )
    # parse the peripherals data just once, in order to improve the performance
    # and return a dictionary of <gene:DataTuple> to use for starting the
    # peripherals optimization
    peripherals_data_generator = parse_peripherals_data(
        network, files, core_files, core_data
    )
    headers = ["source", "lambda", "vmax"] + [tfact for tfact in core.tfacts]

    def dirname_creator(name: str) -> Callable[[SortedList, int], str]:
        # def wrapper(num_solutions: int, num_variables: int) -> str:
        #     return "{}_{}_vars_top{}".format(
        #         name, num_solutions, num_variables
        #     )
        return lambda x, y: "{}_{}_vars_top{}".format(
            name, x[0].number_of_variables, y
        )

    final_handler = DirectoryCSVSolutionsHandler(
        output.directory, float("inf"), headers
    )
    best_genes_solutions = []

    # may Kotlin and Java replace Python in the future
    # Python sucks, so I can't do this in parallel
    # optimize independently for each gene
    for gene, gene_data in peripherals_data_generator:
        logger.info("Starting optimization of gene {}".format(gene))
        iter_function = context.references.iteration_function(
            gene_data, context
        )
        optimization_builder = OptimizationFactory.new_base_optimization(
            context.primary_first.type, iter_function
        )
        prob_factory, start_problem = problem_setup(
            gene_data, context, iter_function
        )
        PeripheralSolution._get_gene_name = lambda x: gene
        handler = DirectoryCSVSolutionsHandler(
            output.directory, output.num_solutions, headers,
            dirname_creator(gene)
        )

        optimization = optimization_builder.build(
            context, prob_factory, start_problem,
            SortedDict({gene: gene_data.reactions}),
            handler, logging.getLogger(__name__)
        )
        solutions = optimization.solve(topol=topology.ring())

        # save the best one in a gene specific directory
        final_handler.handle_solutions(solutions, "{}_best".format(gene))
        best_genes_solutions.append(solutions[0])
        logger.info("Ending optimization of gene {}".format(gene))
    return best_genes_solutions, headers


def prepare_peripherals_job(config: ConfigurationParser, files: InputFiles,
                            core_files: CoreFiles, num_tasks: int
                            ) -> Dict[str, Tuple[str, pd.DataFrame]]:
    # try to parse the network file for seeing if everything works
    network = create_network(files.network, core_files.core_system)

    # for each task, the configuration changes only in the network file.
    # Hidden files are used for the partial network and the specific ini.
    network_frame = pd.read_csv(files.network, sep="\t")
    temp_file = ".lassim_hidden_temp_peripherals_{}.csv"
    temp_config = ".lassim_hidden_temp_config_{}.ini"
    network_subsets = np.array_split(network_frame, num_tasks)
    config_network_dict = {
        temp_config.format(i): (temp_file.format(i), network_subsets[i])
        for i in range(num_tasks)
    }
    for config_file, network_files in config_network_dict.items():
        net_file, net_subset = network_files

        task_config = from_parser_to_builder(config, config_file)
        task_config.add_section(sections.input).add_key_value(
            "network", net_file
        ).add_section(sections.output).add_key_value(
            "best result", net_file
        ).add_section(sections.extra).add_key_value(
            "num tasks", str(1)
        ).build()
        net_subset.to_csv(net_file, sep="\t", index=False)
    return config_network_dict


def start_jobs(config_files: List[str]):
    # For each network file a new set of command line options must be created.
    # If a log file is used, maybe each process should have it independent
    # version.

    processes = []
    for config in config_files:
        python = sys.executable
        program = os.path.abspath(__file__)
        process = subprocess.Popen([
            python, os.path.join(os.getcwd(), program),
            config
        ])
        processes.append(process)
    for process in processes:
        process.wait()


def merge_results(network_files: List[str], output: OutputFiles):
    results = []
    logger = logging.getLogger(__name__)
    for file in network_files:
        try:
            results.append(pd.read_csv(file, sep="\t"))
        except OSError:
            # avoid crashing if results is missing
            logger.error("File {} not found".format(file))
    output_filename = os.path.join(output.directory, output.filename)
    if len(results) > 0:
        merged_result = pd.concat(results, ignore_index=True)
        merged_result.to_csv(output_filename, sep="\t", index=False)
        logger.info("Generated file {}".format(output_filename))


def cleaning_temps(config_files: List[str], network_files: List[str]):
    for file in network_files:
        os.remove(file)
    for config_file in config_files:
        os.remove(config_file)


def lassim_peripherals():
    script_name = "lassim_peripherals"
    # read terminal arguments
    args = default_terminal(script_name, peripheral_configuration_example)

    # extra argument for this script are: name for output file
    config, files, core_files, output, main_args, sec_args, extra = \
        parse_peripherals_config(args.configuration)
    modules = parse_modules_config(args.configuration)

    if extra.num_tasks == 1:
        try:
            solutions, headers = peripherals_job(
                files, core_files, output, main_args, sec_args, modules
            )
            # each solution is the solution for a single gene.
            # merges them into a DataFrame and saves them into the network.csv
            # file for the main process.
            result = pd.concat([solution.get_solution_matrix(headers)
                                for solution in solutions],
                               ignore_index=True)
            result.to_csv(output.filename, sep="\t", index=False)
        except Exception:
            # in this way, the job still ends and doesn't block the root process
            logging.getLogger(__name__).exception(
                "Something weird occurred, ending the job execution..."
            )
            return
    else:
        config_net_dict = prepare_peripherals_job(
            config, files, core_files, extra.num_tasks
        )
        config_files = [config_files
                        for config_files in config_net_dict.keys()]
        network_files = [net_files[0]
                         for net_files in config_net_dict.values()]
        start_jobs(config_files)
        merge_results(network_files, output)
        cleaning_temps(config_files, network_files)


if __name__ == '__main__':
    lassim_peripherals()
