import functools
import logging
from typing import List, Tuple

import pandas as pd

from customs.configuration_custom import peripheral_configuration_example, \
    default_terminal, parse_peripherals_config, parse_modules_config
from lassim_peripherals import prepare_peripherals_job, merge_results, \
    cleaning_temps, peripherals_job

try:
    from mpi4py import MPI
except ImportError:
    logging.getLogger(__name__).exception("Error importing mpi4py")
    exit(1)

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "1.0.0"

DEATH_MESSAGE = "DEATH TO THE EMPIRE!!!"


def start_jobs(communicator, config_files: List[str]
               ) -> Tuple[int, int, List[str]]:
    files = [""] + config_files
    communicator.scatter(sendobj=files, root=0)
    communicator.barrier()

    # collects successes from the other processes
    # the order is rank based!!
    successes = communicator.gather(sendobj=None, root=0)[1:]
    num_success = functools.reduce(
        lambda x, y: x + 1 if y else x, successes, 0
    )
    num_failures = len(successes) - num_success

    # used for knowing which configuration file was successfully executed
    success_config = []
    for i in range(len(successes)):
        if successes[i]:
            success_config.append(config_files[i])
    return num_success, num_failures, success_config


def send_death_signal(communicator):
    """
    Used to send a death signal to all processes. In this way the execution
    can be ended by the root process
    :param communicator: MPI.COMM
    """
    size = communicator.Get_size()
    communicator.scatter(sendobj=[DEATH_MESSAGE for _ in range(size)], root=0)
    communicator.barrier()


def root_peripherals(communicator, name: str):
    args = default_terminal(name, peripheral_configuration_example)

    try:
        config, files, core_files, output, main_args, sec_args, extra = \
            parse_peripherals_config(args.configuration)
        config_network_dict = prepare_peripherals_job(
            config, files, core_files, communicator.Get_size() - 1
        )
        config_files = [config_file
                        for config_file in config_network_dict.keys()]
        network_files = [files[0] for files in config_network_dict.values()]
    except Exception:
        # if anything happen at the job instantiation, kills all the other
        # processes to save resources
        send_death_signal(communicator)
        raise RuntimeError("Killing communications")

    num_succ, num_fails, success_config = start_jobs(communicator, config_files)
    logger = logging.getLogger(__name__)
    logger.info("Number of jobs successfully completed {}".format(num_succ))
    logger.info("Number of jobs failed {}".format(num_fails))

    success_networks = [config_network_dict[c_file][0]
                        for c_file in success_config]
    merge_results(success_networks, output)
    cleaning_temps(config_files, network_files)


def task_peripherals(communicator):
    # sends a True or False based on the completion or not of the its job
    completed = False
    try:
        root_message = communicator.scatter(sendobj=None, root=0)
        if root_message == DEATH_MESSAGE:
            raise RuntimeError(
                "Root process told me to die with this message {}".format(
                    root_message
                ))
        config, files, core_files, output, main_args, sec_args, extra = \
            parse_peripherals_config(root_message)
        modules = parse_modules_config(root_message)
        solutions, headers = peripherals_job(
            files, core_files, output, main_args, sec_args, modules
        )
        result = pd.concat([solution.get_solution_matrix(headers)
                            for solution in solutions],
                           ignore_index=True)
        result.to_csv(output.filename, sep="\t", index=False)
        completed = True
    except Exception:
        logging.getLogger(__name__).exception(
            "Something weird occurred, ending the job execution"
        )
    finally:
        # syncs with the root process and the other tasks
        comm.barrier()
        # sends a True/False if the job is completed
        comm.gather(sendobj=completed, root=0)


if __name__ == '__main__':
    script_name = "lassim_peripherals_mpi"

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    if rank == 0:
        # root process
        root_peripherals(comm, script_name)
    else:
        # task processes
        task_peripherals(comm)
