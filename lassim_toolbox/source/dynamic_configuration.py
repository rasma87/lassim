from argparse import ArgumentParser

from utilities.configuration import ConfigurationBuilder, ConfigurationParser, \
    from_parser_to_builder

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "1.0.0"


# noinspection PyUnresolvedReferences
def parse_configuration(args) -> ConfigurationBuilder:
    import customs.configuration_parser_extensions

    config = ConfigurationParser(args.configuration).define_output_section(
    ).define_logger_section().parse()

    return from_parser_to_builder(config, args.name)


def terminal(script_name: str):
    parser = ArgumentParser(script_name)
    parser.add_argument("configuration", metavar="file.ini",
                        help="Configuration file to modify")
    parser.add_argument("name", metavar="file.ini",
                        help="New name for the configuration file to modify")
    parser.add_argument("directory", metavar="dir",
                        help="Name of the directory to use")
    parser.add_argument("-l", "--log", metavar="file.log",
                        help="Name of the log file to use")

    args = parser.parse_args()

    config = parse_configuration(args)
    config = config.add_section("Output").add_key_value(
        "directory", args.directory
    )
    if args.log is not None:
        config.add_section("Logging").add_key_value("log", args.log)
    config.build()


if __name__ == '__main__':
    name = "dynamic_configuration"
    terminal(name)
