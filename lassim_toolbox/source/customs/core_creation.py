import logging

import numpy as np
from lassim.base_optimization import BaseOptimization
from lassim.factories import OptimizationFactory
from lassim.lassim_context import LassimContext
from lassim.lassim_network import CoreSystem, LassimNetwork
from lassim.problems.core_problem import CoreProblemFactory, CoreProblem
from lassim.type_aliases import Vector, Float, Tuple4V
from sortedcontainers import SortedDict
from typing import Tuple

from data_management.csv_format import parse_network, parse_time_sequence, \
    parse_patient_data, parse_perturbations_data
from utilities.data_classes import InputFiles, CoreData

"""
Set of custom functions for lassim_api creation and general setup of the problem.
Can be considered an example of possible integration of the source/lassim_api module
in an already existing pipeline.
"""

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "1.0.0"


def create_core(network_file: str) -> CoreSystem:
    """
    Creates a CoreSystem instance and logs it by reading the name of the network
    file given as input.

    :param network_file: Path of the file containing the network.
    :return: An instance of the CoreProblem.
    """

    tf_network = parse_network(network_file)
    core = CoreSystem(tf_network)
    logging.getLogger(__name__).info("\n" + str(core))
    return core


def problem_setup(files: InputFiles, context: LassimContext,
                  ) -> Tuple[CoreData, CoreProblemFactory]:
    """
    It setups the lassim_api problem factory for constructing lassim_api problems.

    :param files: Instance of CoreFiles with path to files for lassim_api
        optimization.
    :param context: a LassimContext instance.
    :return: A tuple with the namedtuple containing the data and an instance
        of the problem factory.
    """

    data, sigma, times, y0 = data_parsing(files)
    is_pert_prob, perturbations = data_parse_perturbations(
        files, context.network
    )
    refs = context.references

    # creation of the correct type of problem to solve
    if is_pert_prob and refs.perturbation_function is not None:
        problem_builder = CoreProblemFactory.new_instance(
            (data, sigma, times, perturbations), y0,
            refs.ode_function, refs.perturbation_function,
        )
        logging.getLogger(__name__).info(
            "Created builder for problem with perturbations."
        )
    else:
        problem_builder = CoreProblemFactory.new_instance(
            (data, sigma, times), y0, refs.ode_function
        )
        logging.getLogger(__name__).info(
            "Created builder for problem without perturbations."
        )
    return CoreData(data, sigma, times, perturbations, y0), problem_builder


def data_parsing(files: InputFiles) -> Tuple4V:
    """
    Generate the set of data used in the optimization from the file name.

    :param files: CoreFiles instance with the path to the lassim_api files.
    :return: Vector for mean of data, standard deviation, time series and
        starting values at t0.
    """

    time_seq = parse_time_sequence(files.times)
    data_list = [parse_patient_data(data_file).values
                 for data_file in files.data]

    data_maxes = [np.amax(data, axis=1) for data in data_list]
    maxes = np.array(data_maxes, dtype=Float)
    real_maxes = np.amax(maxes, axis=0)
    np_data_norm = [(data.T / real_maxes).T for data in data_list]
    data_mean_n = np.array(np_data_norm, dtype=Float).mean(axis=0)
    # unbiased, divisor is N - 1
    std_dev = np.std(np.array(np_data_norm, dtype=Float), axis=2)

    data_mean = np.array(data_list, dtype=Float).mean(axis=0)

    return data_mean_n.T, std_dev.mean(axis=0), time_seq, data_mean.T[0].copy()


def data_parse_perturbations(files: InputFiles, network: LassimNetwork
                             ) -> Tuple[bool, Vector]:
    """
    Parse the data for perturbations if they are present. If they are presents,
    checks that their size corresponds to the number of transcription factors
    expected in the lassim_api of the network.

    :param files: CoreFiles instance containing the perturbations file path.
    :param network: The LassimNetwork object containing the CoreSystem.
    :return: True and parsed data if they are present and valid, False and empty
        vector if not.
    """

    core = network.core
    # FIXME - perfect case for Optional.
    if files.perturbations is not None:
        is_present = False
        pert_data = parse_perturbations_data(files.perturbations).values
        # checks data validity
        # checks if data shape is >= to (#tfacts, #tfacts)
        num_tfacts = core.num_tfacts
        if pert_data.shape >= (num_tfacts, num_tfacts):
            tfacts_pert_data = pert_data[:, :num_tfacts]
            # now checks if the data shape of data related to the transcription
            # factors is equal or not to (#tfacts, #tfacts)
            if tfacts_pert_data.shape == (num_tfacts, num_tfacts):
                is_present = True
            else:
                pert_data = np.empty(0)
        else:
            pert_data = np.empty(0)

        return is_present, pert_data
    else:
        return False, np.empty(0)


def optimization_setup(context: LassimContext,
                       problem_builder: CoreProblemFactory,
                       ) -> Tuple[BaseOptimization, CoreProblem]:
    """
    Setup of a BaseOptimization instance, constructing the problem using the
    input LassimContext and the CoreProblemFactory.
    
    :param context: The current working LassimContext.
    :param problem_builder: A CoreProblemFactory for building the corresponding
        problem to solve.
    :return: The BaseOptimization class to use for building the instance that
        will solve the problem and its starting problem to solve.
    """

    main_args = context.primary_opts
    sec_args = context.secondary_opts

    # depending on the secondary arguments, a different optimization instance
    # is created
    main_opt = main_args[0]

    problem = context.references.iteration_function(
        problem_builder, context, SortedDict(), None, prob_init=True
    )[0]

    if len(sec_args) == 1:
        raise NotImplementedError(
            "Secondary algorithm have not been implemented yet. Sorry :("
        )
        # opt_builder = OptimizationFactory.new_multistart_optimization(
        #     main_opt.type, problem_builder, problem, reactions_ids,
        #     iter_function, sec_args[0].type
        # )
    elif len(sec_args) == 0:
        opt_builder = OptimizationFactory.new_base_optimization(
            main_opt.type, context.references.iteration_function
        )
    else:
        raise RuntimeError("How the hell did you get there?!")
    return opt_builder, problem
