import logging
from copy import deepcopy

import numpy as np
from lassim.lassim_context import LassimContext
from lassim.problems.core_problem import CoreProblemFactory, CoreProblem
from lassim.solutions.core_solution import CoreSolution
from lassim.type_aliases import Vector, Float, Tuple2V
from sortedcontainers import SortedDict, SortedList
from typing import List, Tuple, Optional

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "1.0.0"

"""
Set of custom functions for the handling the optimization problem of a lassim.
Can be considered an example on how to integrate source/lassim_api in an existing
pipeline.
"""


def default_bounds(num_tfacts: int, num_react: int
                   ) -> Tuple[List[float], List[float]]:
    """
    Creates a tuple containing as first element the list of lower bounds, and as
    second element the list of upper bounds for the parameter to optimize.
    By default, for lambda/vmax the bounds are (0, 20), while for the k are
    (-20, 20)

    :param num_tfacts: Number of transcription factors in the network.
    :param num_react: Number of reactions between the transcription factors.
    :return: Tuple containing the lower bounds list and upper bounds list.
    """

    lower_bounds = [0.0 for _ in range(0, num_tfacts * 2)]
    upper_bounds = [20.0 for _ in range(0, num_tfacts * 2 + num_react)]
    for _ in range(0, num_react):
        lower_bounds.append(-20.0)
    return lower_bounds, upper_bounds


def generate_reactions_vector(reactions: SortedDict, dt_react=Float
                              ) -> Tuple2V:
    """
    From a reactions map, generates the corresponding numpy vector for
    reactions and its boolean mask. The reaction vector contains #tfacts^2
    elements, and each #tfacts subset represent the list of reactions with
    other transcription factors for one of them. The values are 0 if the
    reaction is not present and numpy.inf if it is.

    :param reactions: Map of transcription factors and their reactions
    :param dt_react: Type of vector values
    :return: ndarray for reactions and its corresponding boolean mask
    """

    reacts = []
    num_tfacts = len(reactions.keys())
    for tfact, reactions in reactions.items():
        vector = np.zeros(num_tfacts, dtype=dt_react)
        vector[reactions] = np.inf
        reacts.append(vector.tolist())
    reacts_flatten = [val for sublist in reacts for val in sublist]
    np_reacts = np.array(reacts_flatten, dtype=dt_react)

    return np_reacts, np_reacts == np.inf


def remove_lowest_reaction(vres: Vector, reactions: SortedDict
                           ) -> Tuple[Vector, SortedDict]:
    """
    Searches in the optimization result vector vres, which reaction as the
    lowest |val| and removes it. From that index, removes the reaction also
    from the reactions map.

    :param vres: Result of the optimization.
    :param reactions: Map of reactions by ids.
    :return: Result without the lowest |val| and the new reactions map
    """

    tfacts_num = len(reactions.keys())
    k_values = vres[tfacts_num * 2:]
    abs_k = np.absolute(k_values)
    min_index = np.argmin(abs_k)
    if min_index.size > 1:
        min_index = min_index[0]
    reactions = deepcopy(reactions)

    # for each transcription factors and its reactions, count is increased
    # until min_index is found, in order to find each reaction to remove. The
    #  main reaction map is not modified because I'm working on the id one
    # that is dynamically built every time
    count = 0
    for i in range(0, tfacts_num):
        count += len(reactions[i])
        if count > min_index:
            # this transcription factor is the one containing the reaction to
            #  remove
            index = min_index - count + len(reactions[i])
            value = reactions[i].pop(index)
            logging.getLogger(__name__).info(
                "Removed connection={} from tf={}".format(value, i)
            )
            return np.delete(vres, min_index + tfacts_num * 2), reactions
    raise IndexError("Index {} to remove not found!!".format(min_index))


def iter_function(factory: CoreProblemFactory, context: LassimContext,
                  solutions: SortedList, last_solution: CoreSolution,
                  prob_init: bool = False
                  ) -> Tuple[Optional[CoreProblem], SortedDict, bool]:
    """
    Custom function for performing an iteration after a completed optimization
    for a certain number of variables in the lassim. This function generates a
    new problem to solve, with one less variable, a new dictionary of the
    reactions and a boolean value in case the optimization should continue or
    not. The reaction removed from the old problem is the one with the lowest
    value in absolute.

    :param factory: Instance of CoreProblemFactory for building a new instance
        of CoreProblem/CoreWithPerturbationsProblem.
    :param context: The current working LassimContext.
    :param solutions: List of CoreSolution sorted by cost function value.
    :param last_solution: Latest solution generated.
    :param prob_init: Boolean value for using the function for the problem 
        initialization. Default value is False.
    :return: If a new iteration can be performed, it returns the new problem,
        its reactions dictionary and True. Otherwise returns None, an empty dict
        and False.
    """

    if prob_init:
        core = context.network.core
        main_opt = context.primary_first
        reactions_ids = SortedDict(core.from_reactions_to_ids())
        new_problem = factory.build(
            dim=(core.num_tfacts * 2 + core.react_count),
            bounds=default_bounds(core.num_tfacts, core.react_count),
            vector_map=generate_reactions_vector(reactions_ids),
            pert_factor=main_opt.pert_factor
        )
        return new_problem, reactions_ids, False
    else:
        react_mask = last_solution.react_mask
        # checks how many true are still present in the reaction mask.
        # If > 0, it means that there's still at least a reaction
        if react_mask[react_mask].size > 0:
            reactions = last_solution.reactions_ids
            reduced_vect, new_reactions = remove_lowest_reaction(
                last_solution.solution_vector, reactions
            )
            new_react, new_mask = generate_reactions_vector(new_reactions)
            num_react = new_react[new_mask].size
            new_problem = factory.build(
                dim=len(new_reactions.keys()) * 2 + num_react,
                bounds=default_bounds(len(new_reactions.keys()), num_react),
                vector_map=(new_react, new_mask), known_sol=[reduced_vect]
            )
            return new_problem, new_reactions, True
        else:
            return None, SortedDict(), False
