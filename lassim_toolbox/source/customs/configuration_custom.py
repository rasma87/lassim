import json
import logging
import os
from argparse import ArgumentParser

import psutil
from lassim.factories import OptimizationFactory
from lassim.lassim_context import OptimizationArgs
from typing import Dict, Tuple, List, Callable, NamedTuple, Any

from utilities.configuration import ConfigurationParser, ConfigurationBuilder
from utilities.data_classes import InputFiles, OutputFiles, CoreFiles, \
    InputExtra, ReferenceInfo
from utilities.logger_setup import LoggerSetup

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "1.0.0"

Sections = NamedTuple(
    "Sections", [("input", str), ("optimization", str), ("output", str),
                 ("logging", str), ("extra", str), ("core", str),
                 ("ode", str), ("perturbations", str), ("solution_class", str),
                 ("iteration", str)]
)
sections = Sections(
    "Input Data", "Optimization", "Output", "Logging", "Extra", "Core Files",
    "ODE", "Perturbations", "SolutionClass", "Iteration"
)


def input_validity_check(files: Dict) -> Dict[str, Any]:
    logger = logging.getLogger(__name__)

    check_file_validity(files["network"], logger)
    if len(files["data"]) < 2:
        logger.error("Expected two or more input files for data.")
        raise RuntimeError("See log for more information.")
    for data_file in files["data"]:
        check_file_validity(data_file, logger)
    check_file_validity(files["times"], logger)

    logger.info("Network file is {}".format(files["network"]))
    logger.info("Data file are {}".format(", ".join(files["data"])))
    logger.info("Time series file is {}".format(files["times"]))

    if files["perturbations"] is not None:
        check_file_validity(files["perturbations"], logger)
        logger.info("Perturbations file is {}".format(files["perturbations"]))

    return files


def output_conversion(output_dict: Dict) -> Dict[str, Any]:
    logger = logging.getLogger(__name__)
    directory = output_dict["directory"]
    if directory is not None and not os.path.isdir(directory):
        os.makedirs(directory)
        logger.info("Created output directory {}".format(directory))

    num_solutions = 1
    if output_dict["num solutions"] is not None:
        num_solutions = int(output_dict["num solutions"])
        if num_solutions < 1:
            logger.error("Number of solutions must be greater than one.")
            num_solutions = 1

    # FIXME change it
    output_filename = "best_result.csv"
    if output_dict["best result"] is not None:
        output_filename = output_dict["best result"]

    logger.info("Number of solutions saved for each iteration is {}".format(
        num_solutions
    ))

    return {
        "directory": directory,
        "num_solutions": int(output_dict["num solutions"]),
        "filename": output_filename
    }


def optimization_conversion(optimization_dict: Dict) -> Dict[str, Any]:
    algorithm = OptimizationFactory.cus_default()
    if optimization_dict["type"] is not None:
        algorithm = optimization_dict["type"].upper()

    params = {}
    if optimization_dict["parameters"] is not None:
        with open(optimization_dict["parameters"]) as params_json:
            params = json.load(params_json)

    cores = psutil.cpu_count()
    if optimization_dict["cores"] is not None:
        cores = int(optimization_dict["cores"])
        if cores < 1:
            raise RuntimeError("Expected at least 1 lassim_api!")

    evolvs = 1
    if optimization_dict["evolutions"] is not None:
        evolvs = int(optimization_dict["evolutions"])
        if evolvs < 1:
            raise RuntimeError("Expected at least 1 evolution!")

    individuals = 1
    if optimization_dict["individuals"] is not None:
        individuals = int(optimization_dict["individuals"])
        if individuals < 1:
            raise RuntimeError("Expected at least 1 individual!")

    pert_factor = 0
    if optimization_dict["perturbation factor"] is not None:
        pert_factor = float(optimization_dict["perturbation factor"])
        if pert_factor < 0 or pert_factor > 1:
            raise RuntimeError("perturbation factor must be between 0 and 1!")

    return {
        "opt_type": algorithm, "params": params, "num_cores": cores,
        "evolutions": evolvs, "individuals": individuals,
        "pert_factor": pert_factor
    }


def extra_conversion(extra_input: Dict) -> Dict[str, Any]:
    num_tasks = int(extra_input["num tasks"])
    if num_tasks < 1:
        raise RuntimeError("number of tasks must be >= 1")
    return {
        "num_tasks": num_tasks
    }


def core_input_check(files: Dict) -> Dict[str, Any]:
    logger = logging.getLogger(__name__)
    check_file_validity(files["core system"], logger)
    check_file_validity(files["perturbations"], logger)
    check_file_validity(files["y0"], logger)

    logger.info("Core System file is {}".format(files["core system"]))
    logger.info("Core Perturbations file is {}".format(files["perturbations"]))
    logger.info("Core y0 file is {}".format(files["y0"]))

    return {
        "core_system": files["core system"],
        "core_pert": files["perturbations"],
        "core_y0": files["y0"]
    }


def reference_conversion(section: str) -> Callable[[Dict], Dict[str, Any]]:
    def wrapper(inputs: Dict) -> Dict[str, Any]:
        path = inputs.get("module path", "")
        return {
            "module_path": path if path is not None else "",
            "module_name": inputs["module name"],
            "reference_name": inputs["reference name"],
            "type": section
        }

    return wrapper


# noinspection PyUnresolvedReferences
def parse_core_config(args) -> Tuple[InputFiles, OutputFiles,
                                     List[OptimizationArgs],
                                     List[OptimizationArgs]]:
    import customs.configuration_parser_extensions

    logger = logging.getLogger(__name__)

    config = ConfigurationParser(args.configuration).define_section(
        sections.input, "network", "data", "times", "perturbations"
    ).define_optimization_section().define_output_section(
    ).define_logger_section()

    config.parse_logger_section(sections.logging, LoggerSetup())

    files = config.parse_section(
        sections.input, InputFiles, input_validity_check
    )
    is_pert = False
    if files.perturbations is not None:
        is_pert = True

    output = config.parse_section(
        sections.output, OutputFiles, output_conversion
    )

    main_opt = config.parse_section(
        sections.optimization, OptimizationArgs, optimization_conversion
    )
    logger.info("Logging Core Optimization arguments...")
    main_opt.log_args(logger, is_pert)

    return files, output, [main_opt], list()


# noinspection PyUnresolvedReferences
def parse_modules_config(configuration: str) -> List[ReferenceInfo]:
    import customs.configuration_parser_extensions

    logger = logging.getLogger(__name__)

    config = ConfigurationParser(configuration).define_module_section(
        sections.ode
    ).define_module_section(sections.perturbations).define_module_section(
        sections.solution_class
    ).define_module_section(sections.iteration)

    mandatory_references = [
        config.parse_section(section, ReferenceInfo,
                             reference_conversion(section))
        for section in [sections.ode, sections.solution_class,
                        sections.iteration]
    ]
    # in this way if new optional references are added in the future, they logic
    # will be present.
    optional_references = [
        config.parse_section(section, ReferenceInfo,
                             reference_conversion(section))
        for section in [sections.perturbations]
    ]
    references = mandatory_references + [refs for refs in optional_references
                                         if refs.reference_name is not None]

    # FIXME
    for ref in references:
        logger.info(ref)
    return references


# noinspection PyUnresolvedReferences
def core_configuration_example(ini_file: str):
    import customs.configuration_builder_extensions
    import customs.core_functions as core_func

    func_dir = os.path.dirname(os.path.abspath(core_func.__file__))

    ConfigurationBuilder(ini_file).add_section(
        sections.input, "Section containing the data for the toolbox"
    ).add_key_value(
        "network", "file", "File representing the network"
    ).add_key_values(
        "data", ["file1", "file2", ".."], "2 or more data time files"
    ).add_key_value(
        "times", "file", "Time instance file compatible with previous data"
    ).add_optional_key_value(
        "perturbations", "file", "Value for the perturbations file"
    ).add_optimization_section().add_module_section(
        sections.ode, "Section containing the ODE function to use",
        "lassim.functions.common_functions_python", "odeint1e8_lassim"
    ).add_module_section(
        sections.perturbations,
        "Section containing the Perturbation function to use",
        "lassim.functions.perturbation_functions_python",
        "perturbation_func_sequential"
    ).add_module_section(
        sections.solution_class, "Section containing the Solution class to use",
        "lassim.solutions.core_solution", "CoreSolution"
    ).add_module_section(
        sections.iteration, "Section containing the Iteration function to use",
        "core_functions", "iter_function", func_dir
    ).add_output_section().add_logger_section().build()

    print("Generated {} as example.".format(ini_file))


# noinspection PyUnresolvedReferences
def parse_peripherals_config(config_file: str) -> Tuple[ConfigurationParser,
                                                        InputFiles,
                                                        CoreFiles, OutputFiles,
                                                        List[OptimizationArgs],
                                                        List[OptimizationArgs],
                                                        InputExtra]:
    import customs.configuration_parser_extensions

    logger = logging.getLogger(__name__)

    config = ConfigurationParser(config_file).define_section(
        sections.input, "network", "data", "times", "perturbations"
    ).define_section(
        sections.core, "core system", "perturbations", "y0"
    ).define_section(
        sections.extra, "num tasks"
    ).define_optimization_section().define_output_section(
    ).define_logger_section()

    config.parse_logger_section(sections.logging, LoggerSetup())

    files = config.parse_section(
        sections.input, InputFiles, input_validity_check
    )
    core_files = config.parse_section(
        sections.core, CoreFiles, core_input_check
    )
    is_pert = False
    if files.perturbations is not None and core_files.core_pert is not None:
        is_pert = True

    extra = config.parse_section(sections.extra, InputExtra, extra_conversion)
    output = config.parse_section(
        sections.output, OutputFiles, output_conversion
    )

    main_opt = config.parse_section(
        sections.optimization, OptimizationArgs, optimization_conversion
    )
    logger.info("Logging Core Optimization arguments...")
    main_opt.log_args(logger, is_pert)

    return config, files, core_files, output, [main_opt], list(), extra


def default_terminal(name: str, helper: Callable[[str], None]):
    parser = ArgumentParser(name)
    parser.add_argument("configuration", metavar="file.ini",
                        help="Configuration file to use for {}".format(name))
    parser.add_argument("-c", "--configuration-helper", action="store_true",
                        help="Creates an example copy of a configuration file"
                             "for {} using the input name".format(name))
    args = parser.parse_args()
    if getattr(args, "configuration_helper"):
        helper(args.configuration)
        exit(0)
    return args


# noinspection PyUnresolvedReferences
def peripheral_configuration_example(ini_file: str):
    import customs.configuration_builder_extensions
    import customs.peripherals_functions as perip_func

    func_dir = os.path.dirname(os.path.abspath(perip_func.__file__))

    ConfigurationBuilder(ini_file).add_section(
        sections.input, "Section containing the data for the toolbox"
    ).add_key_value(
        "network", "file",
        "File containing the network file with the list of peripheral genes"
    ).add_key_values(
        "data", ["file1", "file2"], "2 or more data time files"
    ).add_key_value(
        "times", "file", "Time instance file compatible with previous data"
    ).add_optional_key_value(
        "perturbations", "file", "Value for the peripherals' perturbations file"
    ).add_section(
        sections.core, "Section containing the data of a completed lassim_api"
    ).add_key_value(
        "lassim_api system", "file",
        "File containing the parameters of the lassim_api"
    ).add_optional_key_value(
        "perturbations", "file",
        "Value for the perturbations file of the lassim_api"
    ).add_key_value(
        "y0", "file",
        "File containing the starting point for each transcription factor"
    ).add_section(
        sections.extra, "Extra parameters to use"
    ).add_key_value(
        "num tasks", "1", "Number of parallel tasks to run"
    ).add_optimization_section().add_module_section(
        sections.ode, "Section containing the ODE function to use",
        "lassim.functions.common_functions_python", "odeint1e8_lassim"
    ).add_module_section(
        sections.perturbations,
        "Section containing the Perturbation function to use",
        "lassim.functions.perturbation_functions_python",
        "perturbation_peripheral"
    ).add_module_section(
        sections.solution_class, "Section containing the Solution class to use",
        "lassim.solutions.peripheral_solution", "PeripheralSolution"
    ).add_module_section(
        sections.iteration, "Section containing the Iteration function to use",
        "peripherals_functions", "iter_function", func_dir
    ).add_output_section(
    ).add_logger_section().build()

    print("Generated configuration file example {}".format(ini_file))


def check_file_validity(filename: str, logger: logging.Logger):
    if filename is not None and not os.path.isfile(filename):
        logger.error("File {} doesn't exist.".format(filename))
        raise RuntimeError("See log for more information.")
