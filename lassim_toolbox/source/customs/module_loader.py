import importlib
import os

from typing import Callable

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "1.0.0"


class ModuleLoader:
    __module_cache = {}

    @classmethod
    def load_reference(cls, module_name: str, reference_name: str,
                       path: str = "", path_fail: bool = False,
                       reload: bool = False) -> Callable:
        """
        Loads a reference from the name of a module. Optionally, it can be 
        indicated the path where to search for the module.
        
        :param module_name: Name of the module where to search for the 
            reference.
        :param reference_name: Name of the reference to search in the module.
        :param path: Optional path where to search the module.
        :param path_fail: If True, raises an exception if the path doesn't 
            exist.
        :param reload: If True, reloads the module if it was previously loaded.
        :return: Reference loaded.
        :raise RuntimeError: If path_fail is True and the path doesn't exist.
            If the reference doesn't exist in the module.
        :raise ImportError: If the module can't be loaded.
        """

        path_modified = False
        if os.path.isdir(path):
            import sys
            sys.path.append(path)
            path_modified = True
        else:
            if path_fail:
                raise RuntimeError(
                    "{} doesn't correspond to an existing path".format(path)
                )
        funct = cls.load_reference_from_module(
            module_name, reference_name, reload
        )
        if path_modified:
            import sys
            sys.path.remove(path)
        return funct

    @classmethod
    def load_reference_from_module(cls, module_name: str, reference_name: str,
                                   reload: bool = False) -> Callable:
        """
        Loads a reference from the name of a module. Optionally, it can reload
        the module if it was previously loaded.
        
        :param module_name: Name of the module where to search for the 
            reference.
        :param reference_name: Name of the reference to search in the module.
        :param reload: If True, reloads the module if it was previously loaded.
        :return: Reference loaded.
        :raise RuntimeError: If the reference doesn't exist in the module.
        :raise ImportError: If the module can't be loaded.
        """

        if module_name in cls.__module_cache:
            if reload:
                importlib.reload(cls.__module_cache[module_name])
            module_loaded = cls.__module_cache[module_name]
        else:
            module_loaded = importlib.import_module(module_name)
            cls.__module_cache[module_name] = module_loaded

        if reference_name in module_loaded.__dict__:
            return module_loaded.__dict__[reference_name]
        else:
            raise RuntimeError("Function {} not found in module {}".format(
                reference_name, module_name
            ))
