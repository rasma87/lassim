from customs.configuration_custom import sections
from utilities.configuration import ConfigurationParser

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "1.0.0"


def optimization_section(config: ConfigurationParser) -> ConfigurationParser:
    return config.define_section(
        sections.optimization, "type", "parameters", "cores", "evolutions",
        "individuals", "perturbation factor"
    )


ConfigurationParser.define_optimization_section = optimization_section


def module_section(config: ConfigurationParser, section: str
                   ) -> ConfigurationParser:
    return config.define_section(
        section, "module name", "reference name", "module path"
    )


ConfigurationParser.define_module_section = module_section


def output_section(config: ConfigurationParser):
    return config.define_section(
        sections.output, "directory", "num solutions", "best result"
    )


ConfigurationParser.define_output_section = output_section


def logger_section(config: ConfigurationParser):
    return config.define_section(
        sections.logging, "log", "verbosity"
    )


ConfigurationParser.define_logger_section = logger_section
