INSTALLATION GUIDE
==================

The following installation instructions will detail how to install the **LASSIM** toolbox in an Linux environment. 

**[!]** The Windows OS is not supported.

Before using the LASSIM toolbox
----------------------
Before running the LASSIM toolbox it is necessary to install the library dependecies [Boost](#boost-installation)
and [PyGMO](#pygmo-installation), while [GSL](#gsl-installation) is optional for now.
Use the files in the `examples` folder for testing the toolbox or as an example on how to format your data.

**[!]** Before starting run the following commands:
```
mkdir ~/libraries
```
In this way, all the libraries will be installed inside the directory `libraries` in your home path. Change the name 
of the directory if you want to use another one.

During the installation process, `<home>` will always refer your home directory, which can be seen by using the command
`echo $HOME`.

**[!]** Sometimes it is possible that Boost and/or GSL are already installed in your system. Try to build 
PyGMO against them, but if any error occurs, start considering the possibility of a local installation.

**[!]** All the installation processes assume **clang** as the default compiler, but it is not mandatory.

Anaconda installation
---------------------
For making the installation as simple as possible we use [Anaconda](https://anaconda.org/), a Python based platform containing most of 
the packages required for using this toolbox.

Download [Anaconda3-4.1.1-Linux-x86_64.sh](https://repo.continuum.io/archive/index.html) and install it using the following command:
```
bash Anaconda3-4.1.1-Linux-x86_64.sh
source ~/.bashrc
```
Once the installation is completed, be sure that the Anaconda path is saved in your shell profile, usually inside
`~/.bashrc`, by checking that these lines are inside the file, or add them if they are missing
```
# added by Anaconda3 4.1.1 installer
export PATH="<home>/anaconda3/bin:$PATH"
```

In order to create a specific Python environment for the toolbox, run the following commands
```
conda create -n lassim --clone root
source activate lassim
```
From this point on we use `lassim` as the name of our virtual environment, but it is not mandatory to use it. Now we can 
continue with the [Boost installation](#boost-installation) and [PyGMO installation](#pygmo-installation). 

**[!]** Remember to always run `source activate lassim` to use the virtual environment created here during the toolbox
execution.

Also, you'll need to set the following environment variable, usually by adding them to the `~/.bashrc` file
```
# CUSTOM - anaconda3-4.1.1
export C_INCLUDE_PATH="$HOME/anaconda3/include/python3.5m/:$C_INCLUDE_PATH"
export CPLUS_INCLUDE_PATH="$HOME/anaconda3/include/python3.5m/:$CPLUS_INCLUDE_PATH"
```
Then run the following command in order to activate the updated bash profile
```
source ~/.bashrc
source activate lassim
```

**[Mac OS users]** There are few differences for Mac OS users to keep in mind:
- The installer to download for Anaconda is [Anaconda3-4.1.0-MacOSX-x86_64.pkg](https://repo.continuum.io/archive/index.html)
- Change your default terminal to `bash`
- If `~/.bashrc` is missing, then look for the file `~/.bash_profile`

Boost installation
------------------
The version of boost used for development is the [1.61.0](https://sourceforge.net/projects/boost/files/boost/1.61.0/boost_1_61_0.tar.gz)
but versions newer than this should work fine too. Download `boost_<version>.tar.gz` from 
[boost website](http://www.boost.org), then run the following commands:
```
tar -xvzf boost_<version>.tar.gz
cd boost_<version>
./bootstrap.sh --with-toolset=clang --prefix=<home>/libraries/boost-<version>
./b2 toolset=clang
./b2 install
```

Then, add the following environment variables, usually by adding them to the `~/.bashrc` file
```
# CUSTOM - boost
vboost="<version>"
export LIBRARY_PATH="$LIBRARY_PATH:$HOME/libraries/boost-$vboost/lib"
export CPATH="$CPATH:$HOME/libraries/boost-$vboost"
export LD_LIBRARY_PATH="$HOME/libraries/boost-$vboost/lib:$LD_LIBRARY_PATH"
export PATH="$PATH:$HOME/libraries/boost-$vboost/include:$HOME/libraries/boost-$vboost/lib"
```
Then run the following command in order to activate the updated bash profile
```
source ~/.bashrc
source activate lassim
```

GSL installation (optional)
----------------
The version used for development is the [2.2.1](http://ftp.acc.umu.se/mirror/gnu.org/gnu/gsl/gsl-2.2.1.tar.gz),
but versions newer than this should work fine too. Download `gsl_<version>.tar.gz` from 
[gsl website](http://ftp.acc.umu.se/mirror/gnu.org/gnu/gsl/), then run the following commands:
```
tar -xvzf gsl_<version>.tar.gz
cd gsl_<version>
export CC=clang
export CXX=clang++
./configure --prefix=<home>/libraries/gsl-<version>
make
make check
make install
make installcheck
```
Verify that all the test are successfully passed. Then, add the following environment variables, usually by 
adding them to the `~/.bashrc` file
```
# CUSTOM - gsl
vgsl="<version>"
export LIBRARY_PATH="$LIBRARY_PATH:$HOME/libraries/gsl-$vgsl/lib"
export LD_LIBRARY_PATH="$HOME/libraries/gsl-$vgsl/lib:$LD_LIBRARY_PATH"
export LD_RUN_PATH="$HOME/libraries/gsl-$vgsl/lib:$LD_RUN_PATH"
export PATH="$PATH:$HOME/libraries/gsl-$vgsl/include:$HOME/libraries/gsl-$vgsl/lib"
```
Then run the following command in order to activate the updated bash profile
```
source ~/.bashrc
source activate lassim
```

PyGMO installation
------------------

```
export CC=clang
export CXX=clang++

git clone https://github.com/esa/pagmo.git
cd pagmo
mkdir build
cd build
ccmake ..
```
Enable the following flags:
```
BUILD_MAIN              ON
BUILD_PYGMO             ON
CMAKE_INSTALL_PREFIX    <home>/libraries/pagmo
ENABLE_GSL              ON # leave it to OFF if GSL is not installed
ENABLE_TESTS            ON
INSTALL_HEADERS         ON
```
Or alternatively you can run the following command:
```
cmake -DCMAKE_INSTALL_PREFIX=<home>/libraries/pagmo -DBUILD_MAIN:BOOL=ON -DBUILD_PYGMO:BOOL=ON -DENABLE_GSL:BOOL=ON -DENABLE_TESTS:BOOL=ON -DINSTALL_HEADERS:BOOL=ON ../
```

Then complete the installation with
```
make
make test
make install
```
Make sure to pass all the tests after the call to `make test`

Then, add the following environment variables, usually by adding them to the `~/.bashrc` file
```
# CUSTOM - PyGMO
export LD_LIBRARY_PATH="$HOME/libraries/pagmo/lib:$LD_LIBRARY_PATH"
```
Then run the following command in order to activate the updated bash profile
```
source ~/.bashrc
source activate lassim
```

And run the final tests to see if PyGMO has been correctly installed.
```
python -c "from PyGMO import test; test.run_full_test_suite()"
```

Python Packages
---------------
The only Python package to install is [sortedcontainers](http://www.grantjenks.com/docs/sortedcontainers/index.html), and
it is possible to do this by running the command:
```
source activate lassim
pip install sortedcontainers
```

MPI for Peripherals Optimization (optional)
--------------------------------
The Peripheral Genes Optimization is currently available for single node computation and multiple nodes computation.
In order to use it on multiple nodes, it is necessary to run the following command, in order to install the necessary
packages:
```
conda install --channel mpi4py mpich mpi4py
```

Known Issues
------------
- If you are using Anaconda as Python environment, and after the PyGMO installation you have errors about missing 
**libstdc++.so** or version different from **GLIBCXX_3.4.15**, the command `conda install libgcc` should fix this issue. 
