#!/usr/bin/env bash

cd lassim_api
pip uninstall lassim -y
rm -r build/ dist/ lassim.egg-info/
python setup.py build
python setup.py install